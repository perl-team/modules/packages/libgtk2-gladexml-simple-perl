Source: libgtk2-gladexml-simple-perl
Section: perl
Priority: optional
Build-Depends: debhelper (>= 11)
Build-Depends-Indep: perl, libgtk2-perl, libgtk2-gladexml-perl,
 help2man, libxml-sax-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>
Standards-Version: 4.1.4
Homepage: https://metacpan.org/release/Gtk2-GladeXML-Simple
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libgtk2-gladexml-simple-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libgtk2-gladexml-simple-perl
Testsuite: autopkgtest-pkg-perl

Package: libgtk2-gladexml-simple-perl
Architecture: all
Depends: ${perl:Depends}, ${misc:Depends}, libgtk2-perl, libgtk2-gladexml-perl
Recommends: libxml-sax-perl
Description: clean object-oriented perl interface to Gtk2::GladeXML
 Gtk2::GladeXML::Simple is a module that provides a clean and easy interface
 for Gnome/Gtk2 and Glade applications using an object-oriented syntax. You just
 make Gtk2::GladeXML::Simple your application's base class, have your new call
 SUPER::new, and the module will do the tedious and dirty work for you.
 .
 Gtk2::GladeXML::Simple offers:
  * Signal handler callbacks as methods of your class.
  * Autoconnection of signal handlers.
  * Autocalling of creation functions for custom widgets.
  * Access to the widgets as instance attributes.
